const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);


var users = [];

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
  // res.status(200).send("Hello World!");
});

io.on('connection', (socket) => {
  console.log('A user connected');
  socket.emit("users", users);
  
  socket.on("online", function (data) { 

    console.log("online", data);
    
  });

  socket.on("set-peer-id", function (data) {

    console.log("data", data);

    var find = false;
    users.forEach( (element,i) => {
      if( element.email == data.userEmail ){ 
        find = true;
        if( element.peerId != data.token ){
          users[i].peerId = data.token; 
        }
      } 
    }); 

    if( find == false ) { 
      var user = {
        userId: data.userId,
        username: data.userName,
        email: data.userEmail,
        peerId: data.token,
        channel: null,
        messageInBackground: null
      }
      users.push(user); 
      console.log('set-peer-id: ', data.userId, data.userName, data.userEmail, data.token);       
    } 
    
    io.sockets.emit("users-change", users); 

    console.log("users", users);
    
  });

  socket.on("set-channel", function (data) {
    
    console.log("[LOG] set-channel", data);  

    users.forEach( (element,i) => {
      if( element.userId == data.ID ){ 
        users[i].channel = data.peerId; 
      }  
    }); 

    console.log("users", users);

  });

  socket.on("call", function (data) {
    
    console.log("[LOG] call", data);  

    var user = false; 
    var emisor = false; 
    users.forEach(element => {
      if( element.userId == data.receptor ) user = element;  
      if( element.userId == data.emisor ) emisor = element;   
    });

    console.log("call-" + user.peerId, user);  
    io.sockets.emit("call-" + user.peerId , { user, emisor, data} );  

  });

  socket.on("accept-call", function (data) {
    console.log("[LOG] accept CALL", data);   

    io.sockets.emit("accept-call-" + data.peerIdCanal , data.peerId );  

    
  });

  socket.on("end-call", function (data) {
    console.log("[LOG] END CALL", data);  

    io.sockets.emit("end-call-" + data.peerId , data.ID ); 

  });

  socket.on("reject-call", function (data) {
    console.log("[LOG] REJECT CALL", data);  

    io.sockets.emit("reject-call-" + data.peerIdCanal , data.id_cat );  

  });

  socket.on("message-background", function (data) { 

    console.log("[LOG] MESSAGE BACKGROUND", data);  

    users.forEach( (element,i) => {
      if( element.userId == data.user_id ){ 
        users[i].messageInBackground = data.link; 
      }  
    }); 
    
    console.log("users", users); 

    io.sockets.emit("message-background-" + data.user_id , data );  

  });

});

server.listen(3000, () => {
  console.log('listening on *:3000');
});
